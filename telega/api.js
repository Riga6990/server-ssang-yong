const https = require('https');

module.exports = {
    recipient: null,
    message: null,
    token: null,
    endpoint: 'https://api.telegram.org/bot%token/sendMessage?chat_id=%chatId&text=%message',

    setToken: function (token) {
        this.token = token;
    },

    setRecipient: function (chatId) {
        this.recipient = chatId;
    },

    setMessage: function (message) {
        this.message = message;
    },

    send: function ( res ) {
        console.log('send to telegram has bin called')
        let endpointUrl = this.endpoint
            .replace('%token', encodeURIComponent(this.token))
            .replace('%chatId', encodeURIComponent(this.recipient))
            .replace('%message', encodeURIComponent(this.message));

        // Create Http request

        // Send the request call

        console.log(endpointUrl);
        https.get(endpointUrl, (resp) => {
            let data = '';
            // A chunk of data has been recieved.
            resp.on('data', (chunk) => {
                data += chunk;
            });

            // The whole response has been received. Print out the result.
            resp.on('end', () => {
                res.end({
                    success: true,
                });
            });
        }).on("error", (err) => {
            res.status(400);
            res.send({success: false})
        });
    }
};