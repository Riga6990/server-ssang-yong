const router = require('express').Router();
const telega = require('../telega');
const cors = require('cors');

/* GET users listing. */
router.post('/', cors(), async function(req, res, next) {
    res.header({
        'withCredentials': true,
        'Content-Type': 'application/json;charset=utf-8',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept'
    });
    if(req.body.message) {
        telega(req.body.message, res);
    } else {
        res.status(400);
        res.send({
            status: false,
        })
    }
    res.end();
});

router.get('/', cors(), function(req, res, next) {
    res.send('get');
    res.end();
});

module.exports = router;
